<?php get_header(); ?>
<div class="row blog_row">

<section class="post_single col-md-8 col-xs-12">
		<?php if (have_posts()) : ?>

 			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>

			<?php /* If this is a category archive */ if (is_category()) { ?>
				<header><h2>Archive for the &#8216;<?php single_cat_title(); ?>&#8217; Category</h2></header>

			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>
				<header><h2>Posts Tagged &#8216;<?php single_tag_title(); ?>&#8217;</h2></header>

			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
				<header><h2>Archive for <?php the_time('F jS, Y'); ?></h2></header>

			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
				<header><h2>Archive for <?php the_time('F, Y'); ?></h2></header>

			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
				<header><h2>Archive for <?php the_time('Y'); ?></h2></header>

			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
				<header><h2>Author Archive</h2></header>

			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
				<header><h2>Blog Archives</h2></header>
			
			<?php } ?>
			<div class="border_out">
			<div class="border">
			<div class="background">
			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

			<?php while (have_posts()) : the_post(); ?>
			
				<div <?php post_class() ?>>
				
					<header><h2 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2></header>
					
					<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

					<div class="entry">
						<?php the_content(); ?>
					</div>

				</div>

			<?php endwhile; ?>

			<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>
			</div>
			</div>
			</div>
	<?php else : ?>

		<header><h2>Nothing found</h2></header>

	<?php endif; ?>
</section>

<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>