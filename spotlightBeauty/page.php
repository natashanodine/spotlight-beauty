<?php get_header(); ?>
<div class="row blog_row body">
<section class=" col-md-9 col-xs-12">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			
		<div class="post post_single" id="post-<?php the_ID(); ?>">
			<header class="header" >
				<h2><?php the_title(); ?></h2>
			</header>
			<div class="border_out">
			<div class="border">
			<div class="post_entry">
			
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

			<div class="entry">

				<?php the_content(); ?>

				<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>

			</div>

			<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
			<?php comments_template(); ?>
	</div>
	</div>
	</div>
		</div>
		
		
		<?php endwhile; endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>