<?php get_header(); ?>
<div class="row blog_row">
<section class="p col-md-9 col-xs-12">
	<?php if (have_posts()) : ?>

		<h2>Search Results</h2>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

		<?php while (have_posts()) : the_post(); ?>
			<div class="border_out">
			<div class="border">
			<div class="post_single background"<?php post_class() ?> id="post-<?php the_ID(); ?>">
				
				<header class="header" >
					<h2><?php the_title(); ?></h2>
				</header>

				<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>

				<div class="entry">
					<?php the_excerpt(); ?>
				</div>

			</div>
			</div>
			</div>

		<?php endwhile; ?>

		<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>No posts found.</h2>

	<?php endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>