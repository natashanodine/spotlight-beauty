<?php get_header(); ?>

<div class="row blog_row body">
<section class=" col-md-9 col-xs-12">
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<div class="blog_post"<?php post_class() ?> id="post-<?php the_ID(); ?>">

			<header class="header">
				<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
			</header>
			<div class="border_out">
			<div class="border">
			<div class="post_entry">
			<?php include (TEMPLATEPATH . '/inc/meta.php' ); ?>
			
			<article class="entry">
				<p><?php the_content(); ?></p>
			</article>

			<div class="postmetadata">
				<p><?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>
			</div>
			</div>
			</div>
		</div>

	<?php endwhile; ?>

	<?php include (TEMPLATEPATH . '/inc/nav.php' ); ?>

	<?php else : ?>

		<h2>Not Found</h2>

	<?php endif; ?>
</section>
<?php get_sidebar(); ?>
</div>
<?php get_footer(); ?>