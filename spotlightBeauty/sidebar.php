<aside>
	<div class="sidebar col-md-3 col-xs-12">
    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar Widgets')) : else : ?>
		<header>
			<h4>Search</h4>
		</header>
    	<?php get_search_form(); ?>
  
		<header>
    	<h4>Archives</h4>
		</header>
    	<ul>
    		<?php wp_get_archives('type=monthly'); ?>
    	</ul>
		<header>
        <h4>Categories</h4>
		</header>
        <ul>
    	   <?php wp_list_categories('show_count=1&title_li='); ?>
        </ul>
    	<?php wp_list_bookmarks(); ?>
    
    	<header>
    	<h4>Subscribe</h4>
		</header>
    	<ul>
    		<li><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
    		<li><a href="<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
    	</ul>
	
	<?php endif; ?>
	
		
					
				
		
					
			
	
	</div>
	
	
</aside>