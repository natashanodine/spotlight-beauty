<?php include ('header_spanish.php'); ?>
<a class="hash" name="index"></a>



	
		<div class="row wellcome" >
			<div class="col-md-12">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/sandra-villacres.jpg" class="img-responsive center-blok"/>
			</div>
		</div> 



	
		<div class ="body">
			
			<div class="row header_img">
				<div class="col-md-12">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/sandra-villacres-samborondon-miami.png" >
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-12 header">
					<h1>Sandra Villacres</h1>
					<h2>Esteticista Medica<br>y<br>Especializada en Laser<br>CCE - CME </h2>
					<p>"Ella cree que su mayor recompensa proviene de poder ayudar a sus pacientes a sentirse más seguros con su apariencia utilizando las últimas tecnologías no invasivas y excelentes productos"</p>
				</div>
			</div>
			
			<a class="hash" name="treatments"></a>
			
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Tratamientos</h1></header>
				</div>
			</div>
				
				
			<div class="row treatment right-col">
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-samborondon-guayaquil-ecuador.jpg" >				
				</div>
				
				
				<div class="col-md-7 col"> 
				
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
						
						<img data-toggle="modal" data-target="#ultherapy" src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" >
						<div class="modal fade" id="ultherapy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">Terapia de ultasonido</h4>
								</div>
								<div class="modal-body">
								<article class="video_modal">
									<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/jwk0RPhu1BM" frameborder="0"></iframe>
								</article>
								</div>
							  
							</div>
						  </div>
						</div>
							
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">					
							<header><h2>Terapia de Ultrasonido</h2></header>
							<p class="text_title" style="font-weight:bold">Tratamiento de lifting facial no-quir&uacute;rgico de larga duraci&oacute;n.</p>
							<p>Tratamiento de ultrasonido focalizado que realiza un efecto tensor en el tejido conectivo dando una apariencia juvenil y definiendo los rasgos faciales.</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#liposonix" >
							<div class="modal fade" id="liposonix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Liposonic</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/U67CHXLRXCQ" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Liposonic</h2></header>
						<p class="text_title" style="font-weight:bold">Perder 1 talla en un tratamiento de 1 hora. </p>
						<p>Tratamiento de cuerpo utilizando ultrasonido focalizado que penetra en el tejido adiposo (grasa) matando las c&eacute;lulas de grasa y reduciendo medidas.</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#smart_skin">
							<div class="modal fade" id="smart_skin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Smartskin CO2</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/PHs2tDaZnlc" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>CO2 Fractional Laser</h2></header>
							<p class="text_title" style="font-weight:bold">Rejuvenecimiento de la piel para lograr una piel perfecta.</p>
							<p>Efecto tensor a trav&eacute;s de la ablaci&oacute;n de la dermis ayudando a mejorar la textura y color de la piel. Recomendado para cicatrices de acn&eacute; y foto envejecimiento. </p>
						</article>
					</div>
				
				</div>
			</div>
			
			
			
			
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Tratamientos</h1></header>
				</div>
			</div>
			
			<div class="row treatment left-col">
			
				<div class="col-md-7">
				
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8  treatment_box">
							<header><h2>Hydrafacial MD</h2></header>
							<p class="text_title" style="font-weight:bold">Exfolia, hidrata e ilumina la piel. </p>
							<p>Limpieza profunda de la piel utilizando &aacute;cidos de grado m&eacute;dico para remover las c&eacute;lulas muertas de la piel. Adem&aacute;s tambi&eacute;n hidratando con &aacute;cido hialuronico dejando un piel tersa y radiante.</p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#hydrafacial">
							<div class="modal fade" id="hydrafacial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Hydrafacial MD</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/B7-sDavQ4HY" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8  treatment_box">
							<header><h2>Microagujas</h2></header>
							<p class="text_title" style="font-weight:bold">Estimula la producci&oacute;n de col&aacute;geno y la elastina. </p>
							<p>Realiza una pequena herida en la piel estimulando la produccion de colageno, que ayuda a cerrar poros, reducir manchas y lineas de exprecion.</p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#microneedle">
							<div class="modal fade" id="microneedle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Microagujas</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/kK5qfMETMzc" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
					
					</div>				
					
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>VI-Peel</h2></header>					
							<p class="text_title" style="font-weight:bold">El mejor peel para reducir la pigmentaci&oacute;n, el acn&eacute; y el envejecimiento de la piel. </p>
							<p>Es ideal para revertir el da&ntilde;o solar y los signos de envejecimiento . Mejora dram&aacute;ticamente el tono y la textura de la piel. Estimula la producci&oacute;n de col&aacute;geno y elastina </p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#vipeel">
							<div class="modal fade" id="vipeel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">VI-Peel</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/rdWfTcHZjMw" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
					
					</div>
					
				</div>	
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-miami.jpg" >				
				</div>
				
			
			</div>		
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Tratamientos</h1></header>
				</div>
			</div>
			
			<div class="row treatment right-col">
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-tratamientos-belleza-samborondon-miami.jpg" >				
				</div>
				
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#oxygen">
							<div class="modal fade" id="oxygen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Oxigeno Facial</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/p9l_ixoJkN0" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Oxigeno Facial</h2></header>
							<p class="text_title" style="font-weight:bold">Ilumina tu piel con vitaminas y p&eacute;ptidos.</p>
							<p>La infusi&oacute;n de ox&iacute;geno y vitaminas con &aacute;cido hialur&oacute;nico en la piel estimula la producci&oacute;n de col&aacute;geno, lo que ayuda restaurar el volumen.</p>

						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#led">
							<div class="modal fade" id="led" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Luces LED</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/bBI6r55IpQM" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Luces LED</h2></header>
							<p class="text_title" style="font-weight:bold">Tecnologia LED para mejorar su piel. </p>
							<p>La terapia de LED utiliza ondas de color espec&iacute;ficas de luz que penetran en la piel a diferentes profundidades. Esto es un tratamiento de anti-envejecimiento, ayudando a normalizar el desequilibrio celular, mejorando el tono y la claridad de la piel</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#hair_removal">
							<div class="modal fade" id="hair_removal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Depilacion Laser</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/dGyUY4E9Yh8" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Depilacion Laser</h2></header>
							<p class="text_title" style="font-weight:bold">La tecnolog&iacute;a m&aacute;s segura para eliminar el vello no deseado </p>
							<p>Reducci&oacute;n permanente del vello facial y corporal con excelentes resultados, sin dolor, r&aacute;pidos y f&aacute;ciles con poco o ning&uacute;n tiempo de inactividad.</p>
						</article>
					</div>
					
				</div>
				
			</div>
			
			
			
			
			
		<a class="hash" name="contacts"></a>
			<hr>
			<div class="row titles-row contacts_row">
				<div class="col-md-12 titles">
					<header><h1>Contacts</h1></header>
				</div>
			</div>

		<div class="row contacts"  >
			<div class="col-md-4 col-xs-12">
				<div class=" row logo">
					<div class="col-md-12">
						<img  src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/logo-spotlightbeauty-small_b.png"/>
					</div>
				</div>
				<div class="row buttons">
					<div class="col-md-12">
						<button type="button" class="btn btn-default button buttons"  data-toggle='modal' data-target='#contactModal'>SEND EMAIL</button>
						<div class='modal fade' id='contactModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
							<div class='modal-dialog'>
								<div class='modal-content'>
									<div class='modal-header'>
										<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
										<p class='modal-title' id='myModalLabel'>Escribenos y estaremos en contacto contigo</p>
									</div>
									<div class='modal-body'>
										<form role='form' class='col-centered' action='form.php' method='post'>
											<div class='form-group col-centered'>
												<label for='usr' >Nombre:</label>
												<input type='text' class='form-control' id='usr' placeholder='Nombre' name='name' required >
											</div>
											<div class='form-group col-centered'>
												<label for='tel' >Telefono:</label>
												<input type='text' class='form-control' id='tel' placeholder='Telefono' name='tel'>
											</div>
											<div class='form-group col-centered'>
												<label for='email' >Email:</label>
												<input type='email' class='form-control' id='email' placeholder='Email' name='email' required >
											</div>	
											<div class='form-group col-centered'>
												<label for='comment' >Mensaje:</label>
												<textarea class='form-control' rows='2' name='comment' placeholder='Mensaje' required ></textarea>
												
											</div>
											<input class='btn checkout button' value='ENVIAR' type='submit' name="submit">		
										</form>
									</div>	  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 ">	
			
			
				<div class="row">
				
				<div class="col-md-4 col-xs-4 dir-decoration-spotlightbeautymd">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/contacts-decoration.png"/>
				</div>
				<div class="col-md-8 col-xs-8">
				<div class="contact_detail">
					<h3>Ecuador - Samborondon </h3>
					<p>Tel: <a href="tel:+5939980757721">0980757721</a> - <a href="tel:+59346045980">(04) 6045980</a></p>
					<p>Edifio del Portal oficina #204</p>
					
				</div> 	
				<div class="dir-decoration">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/contacts-decoration-2.png"/>
				</div>
				<div class="contact_detail">
					<h3 >Miami - Brickell </h3>
					<p>Tel: <a href="tel:+13055250432">305-525-0432</a> - <a href="tel:+13059331838">305-933-1838</a></p>				
					<p>350 South Miami Ave, Units C-E<br>con</p><p><a href="http://drgcosmeticsurgery.com/" target="blank">Dr. Sam Gershenbaum</a></p>
				</div> 
				</div> 
				</div> 
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 social-icon">
						<a href="https://www.facebook.com/spotlightbeautymd" target="blank"><img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/social-icons/facebook.jpg"/></a>
					
						<a href="https://www.instagram.com/spotlightbeautymd/"  target="blank"><img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/social-icons/instagram.jpg"/></a>
					</div>
				</div> 
				
				
			</div>
			<div class="col-md-4 col-xs-12 maps-col">
				<div class="contact-map">
					<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3987.0369557705744!2d-79.86753368532493!3d-2.1395146377552257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x902d6ce53d2f6ac3%3A0x27c19f5af11c4f80!2sSpotlight+Beauty+EC!5e0!3m2!1sen!2sec!4v1516223594551" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="contact-map">
					<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3987.0369557705744!2d-79.86753368532493!3d-2.1395146377552257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x902d6ce53d2f6ac3%3A0x27c19f5af11c4f80!2sSpotlight+Beauty+EC!5e0!3m2!1sen!2sec!4v1516223594551" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div> 	
		
		<hr>
		<div class="row titles-row">
			<div class="col-md-12 titles">
				<header><h1>Productos</h1></header>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/hydrafacial.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/colorscience.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/viaesthetics.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/obagi.png"/>
			</div>
		</div>
		
	
	</div> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88300496-1', 'auto');
  ga('send', 'pageview');

</script>
		<script	src="http://maps.googleapis.com/maps/api/js"></script>

		<script src="js/map.js"></script>
	
	
</body>
	<?php include ('footer.php'); ?>