<?php include ('tratamientos-belleza-samborondon-miami/header.php'); ?>
<a class="hash" name="index"></a>



	
		<div class="row wellcome" >
			<div class="col-md-12">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/sandra-villacres.jpg" class="img-responsive center-blok"/>
			</div>
		</div> 



	
		<div class ="body">
			
			<div class="row header_img">
				<div class="col-md-12">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/sandra-villacres-samborondon-miami.png" >
				</div>
			</div>
					
			<div class="row">
				<div class="col-md-12 header">
					<h1>Sandra Villacres</h1>
					<h2>Certified Medical Aesthetician<br>and<br>Laser Specialist<br>CCE - CME </h2>
					<p>"She believes that her greatest reward comes from being able to help her patients feel more confident with their appearance using the latest non-invasive technoligies and excelent products"</p>
				</div>
			</div>
			
			<a class="hash" name="treatments"></a>
			
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Treatments</h1></header>
				</div>
			</div>
				
				
			<div class="row treatment right-col">
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-samborondon-guayaquil-ecuador.jpg" >				
				</div>
				
				
				<div class="col-md-7 col"> 
				
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
						
						<img data-toggle="modal" data-target="#ultherapy" src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" >
						<div class="modal fade" id="ultherapy" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						  <div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									<h4 class="modal-title" id="myModalLabel">Ultrasound Therapy</h4>
								</div>
								<div class="modal-body">
								<article class="video_modal">
									<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/jwk0RPhu1BM" frameborder="0"></iframe>
								</article>
								</div>
							  
							</div>
						  </div>
						</div>
							
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">					
							<header><h2>Ultrasound Therapy</h2></header>
							<p class="text_title" style="font-weight:bold">A nonsurgical long lasting lifting.</p>
							<p>Focused ultrasound treatment that produces a tightening effect in the connective tissue giving a youthful appearance and defining the facial features.</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#liposonix" >
							<div class="modal fade" id="liposonix" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Liposonic</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/U67CHXLRXCQ" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Liposonic</h2></header>
							<p class="text_title" style="font-weight:bold">Lose 1 inch in a 1 hour treatment.</p>
							<p>Body treatment using focalized ultrasound that penetrates into the adipose (fat) tissue, killing the fat cells and reducing measurements.</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#smart_skin">
							<div class="modal fade" id="smart_skin" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Smartskin CO2</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/PHs2tDaZnlc" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>CO2 Fractional Laser</h2></header>
							<p class="text_title" style="font-weight:bold">Skin resurfacing to achieve a perfect skin.</p>
							<p>Lifting effect through ablation of the dermis helping to improve the texture and color of the skin. Recommended for acne scars and photo aging.</p>
						</article>
					</div>
				
				</div>
			</div>
			
			
			
			
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Treatments</h1></header>
				</div>
			</div>
			
			<div class="row treatment left-col">
			
				<div class="col-md-7">
				
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8  treatment_box">
							<header><h2>Hydrafacial MD</h2></header>
							<p class="text_title" style="font-weight:bold">Exfoliates, hydrates and glow.</p>
							<p>Deep cleansing of the skin using medical-grade acids to remove dead skin cells. In addition, it also moisturizes with hyaluronic acid leaving a smooth and radiant skin.</p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#hydrafacial">
							<div class="modal fade" id="hydrafacial" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Hydrafacial MD</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/B7-sDavQ4HY" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8  treatment_box">
							<header><h2>Microneedle</h2></header>
							<p class="text_title" style="font-weight:bold">Stimulates new collagen and elastin.</p>
							<p>It makes a small wound on the skin stimulating the production of collagen, which helps to close pores, reduce spots and fine lines.</p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#microneedle">
							<div class="modal fade" id="microneedle" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Microneedle</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/kK5qfMETMzc" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
					
					</div>				
					
					<div class="row">
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>VI-Peel</h2></header>					
							<p class="text_title" style="font-weight:bold">The best peel for pigmentation, acne and aging skin.</p>
							<p>Great for reversing sun damage and the signs of aging. Excellent for cleaning acne and improving acne scars. It stimulates collagen and elastin production</p>
						</article>
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#vipeel">
							<div class="modal fade" id="vipeel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">VI-Peel</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/rdWfTcHZjMw" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
					
					</div>
					
				</div>	
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-miami.jpg" >				
				</div>
				
			
			</div>		
			<hr>
			<div class="row titles-row">
				<div class="col-md-12 titles">
					<header><h1>Treatments</h1></header>
				</div>
			</div>
			
			<div class="row treatment right-col">
				<div class="col-md-5 col img_decoration"> 
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/spotlight-beauty-tratamientos-belleza-samborondon-miami.jpg" >				
				</div>
				
				<div class="col-md-7">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4 video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#oxygen">
							<div class="modal fade" id="oxygen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Oxygen Facial</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/p9l_ixoJkN0" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Oxygen Facial</h2></header>
							<p class="text_title" style="font-weight:bold">Glow your skin with vitamins and peptides.</p>
							<p>The infusion of oxygen and vitamins with hyalouronic acid into the skin encourages the production of collagen, which helps plump up facial features and restore volume.</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#led">
							<div class="modal fade" id="led" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">LED Lights</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
											<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/bBI6r55IpQM" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>LED Lights</h2></header>
							<p class="text_title" style="font-weight:bold">LED technology to improve your skin.</p>
							<p>LED therapy uses specific colour wavelengths of light that penetrate the skin at varying depths. This stimulates the anti-ageing process, helps to normalise cellular imbalance and improves skin tone and clarity</p>
						</article>
					</div>
					
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4  video">
							<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/video.png" data-toggle="modal" data-target="#hair_removal">
							<div class="modal fade" id="hair_removal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Laser Hair Removal</h4>
									</div>
									<div class="modal-body">
									<article class="video_modal">
										<iframe id="video" class="youtube-player" type="text/html" src="https://www.youtube.com/embed/dGyUY4E9Yh8" frameborder="0"></iframe>
									</article>
									</div>
								  
								</div>
							  </div>
							</div>
						</div>
						<article class="col-md-8 col-sm-8 col-xs-8 treatment_box">
							<header><h2>Laser Hair Removal</h2></header>
							<p class="text_title" style="font-weight:bold">The safest technology for removing unwanted hair</p>
							<p>Permanent reduction of facial and body hair with excellent results, no pain, fast, easy treatments with little to no downtime</p>
						</article>
					</div>
					
				</div>
				
			</div>
			
			
			
			
			
		<a class="hash" name="contacts"></a>
			<hr>
			<div class="row titles-row contacts_row">
				<div class="col-md-12 titles">
					<header><h1>Contacts</h1></header>
				</div>
			</div>

		<div class="row contacts"  >
			<div class="col-md-4 col-xs-12">
				<div class=" row logo">
					<div class="col-md-12">
						<img  src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/logo-spotlightbeauty-small_b.png"/>
					</div>
				</div>
				<div class="row buttons">
					<div class="col-md-12">
						<button type="button" class="btn btn-default button"  data-toggle='modal' data-target='#contactModal'>SEND EMAIL</button>
						<div class='modal fade' id='contactModal' tabindex='-1' role='dialog' aria-labelledby='myModalLabel' aria-hidden='true'>
							<div class='modal-dialog'>
								<div class='modal-content'>
									<div class='modal-header'>
										<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
										<p class='modal-title' id='myModalLabel'>Write and we will contact you</p>
									</div>
									<div class='modal-body'>
										<form role='form' class='col-centered' action='tratamientos-belleza-samborondon-miami/form.php' method='post'>
											<div class='form-group col-centered'>
												<label for='usr' >Name:</label>
												<input type='text' class='form-control' id='usr' placeholder='Name' name='name' required >
											</div>
											<div class='form-group col-centered'>
												<label for='tel' >Phone:</label>
												<input type='text' class='form-control' id='tel' placeholder='Phone' name='tel'>
											</div>
											<div class='form-group col-centered'>
												<label for='email' >Email:</label>
												<input type='email' class='form-control' id='email' placeholder='Email' name='email' required >
											</div>										
											<div class='form-group col-centered'>
												<label for='comment' >Message:</label>
												<textarea class='form-control' rows='2' name='comment' placeholder='Message' required ></textarea>
												
											</div>
											<input class='btn checkout button' value='ENVIAR' type='submit' name="submit">		
										</form>
									</div>	  
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-xs-12 ">	
			
			
				<div class="row">
				
				<div class="col-md-4 col-xs-4 dir-decoration-spotlightbeautymd">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/contacts-decoration.png"/>
				</div>
				<div class="col-md-8 col-xs-8">
				<div class="contact_detail">
					<h3>Ecuador - Samborondon </h3>
					<p>Tel: <a href="tel:+5939980757721">0980757721</a> - <a href="tel:+59346045980">(04) 6045980</a></p>
					<p>Edifio del Portal oficina #204</p>
					
				</div> 	
				<div class="dir-decoration">
					<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/contacts-decoration-2.png"/>
				</div>
				<div class="contact_detail">
					<h3 >Miami - Brickell </h3>
					<p>Tel: <a href="tel:+13055250432">305-525-0432</a> - <a href="tel:+13059331838">305-933-1838</a></p>				
					<p>350 South Miami Ave, Units C-E<br> with</p><p><a href="http://drgcosmeticsurgery.com/" target="blank">Dr. Sam Gershenbaum</a></p>
				</div> 
				</div> 
				
				</div> 
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12 social-icon">
						<a href="https://www.facebook.com/spotlightbeautymd" target="blank"><img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/social-icons/facebook.jpg"/></a>
					
						<a href="https://www.instagram.com/spotlightbeautymd/"  target="blank"><img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/social-icons/instagram.jpg"/></a>
					</div>
				</div> 
				
				
			</div>
			<div class="col-md-4 col-xs-12 maps-col">
				<div class="contact-map">
					<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3987.0369557705744!2d-79.86753368532493!3d-2.1395146377552257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x902d6ce53d2f6ac3%3A0x27c19f5af11c4f80!2sSpotlight+Beauty+EC!5e0!3m2!1sen!2sec!4v1516223594551" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="contact-map">
					<iframe class="map" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3987.0369557705744!2d-79.86753368532493!3d-2.1395146377552257!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x902d6ce53d2f6ac3%3A0x27c19f5af11c4f80!2sSpotlight+Beauty+EC!5e0!3m2!1sen!2sec!4v1516223594551" width="400" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</div>
		</div> 	
		
		<hr>
		<div class="row titles-row">
			<div class="col-md-12 titles">
				<header><h1>Products</h1></header>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/hydrafacial.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/colorscience.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/viaesthetics.png"/>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6 products">
				<img src="http://vilcabamba-hotel.com/spotlightmd/images-tratamientos-belleza-esteticos-samborondon-miami/products/obagi.png"/>
			</div>
		</div>
		
		
		
		
	
	</div> 
	
		
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js"></script>
	
	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88300496-1', 'auto');
  ga('send', 'pageview');

</script>
		<script	src="http://maps.googleapis.com/maps/api/js"></script>

		<script src="js/map.js"></script>
	
	
</body>
	<?php include ('tratamientos-belleza-samborondon-miami/footer.php'); ?>