var Ecua=new google.maps.LatLng(-2.130598, -79.860539);
		var Samb=new google.maps.LatLng(-2.128101, -79.864553);
		var Oliv=new google.maps.LatLng(-2.169655, -79.943216);

		var Aventura=new google.maps.LatLng(25.770608, -80.194128);
		var Brickell=new google.maps.LatLng(25.956215, -80.146310);
		var Hampshire=new google.maps.LatLng(42.716823, -71.505293);

		function initialize()
		{
			var mapProp = {
			  center:Ecua,
			  zoom:11,
			  mapTypeId:google.maps.MapTypeId.HYBRID
			  };

			  var mapProp1 = {
			  center:Aventura,
			  zoom:11,
			  mapTypeId:google.maps.MapTypeId.HYBRID
			  };
			  

			  var mapPropBrickell = {
			  center:Brickell,
			  zoom:11,
			  mapTypeId:google.maps.MapTypeId.HYBRID
			  };
			  var mapPropHampshire = {
			  center:Hampshire,
			  zoom:11,
			  mapTypeId:google.maps.MapTypeId.HYBRID
			  };
			  
			  
			var mapEcua=new google.maps.Map(document.getElementById("googleMapEcua"),mapProp);
			var mapAventura=new google.maps.Map(document.getElementById("googleMapAventura"),mapProp1);
			var mapBrickell=new google.maps.Map(document.getElementById("googleMapBrickell"),mapPropBrickell);
			var mapHampshire=new google.maps.Map(document.getElementById("googleMapHampshire"),mapPropHampshire);

			var markerSamb=new google.maps.Marker({
			  position:Samb,
			  });

			markerSamb.setMap(mapEcua);

			var infowindowSam = new google.maps.InfoWindow({
			  content:"Samborondon - Club Deportivo Diana Quintana"
			  });

			google.maps.event.addListener(markerSamb, 'click', function() {
			  infowindowSam.open(mapEcua,markerSamb);
			  });

			  
			var markerAventura=new google.maps.Marker({
			  position:Aventura,
			  });

			markerAventura.setMap(mapAventura);

			var infowindow = new google.maps.InfoWindow({
			  content:"Aventura - Aventura"
			  });

			google.maps.event.addListener(markerAventura, 'click', function() {
			  infowindow.open(mapAventura,markerAventura);
			  });
			  
			  var markerBrickell=new google.maps.Marker({
			  position:Brickell,
			  });

			markerBrickell.setMap(mapBrickell);

			var infowindow = new google.maps.InfoWindow({
			  content:"Brickell"
			  });

			google.maps.event.addListener(markerBrickell, 'click', function() {
			  infowindow.open(mapBrickell,markerBrickell);
			  });
			  
			  
			  var markerHampshire=new google.maps.Marker({
			  position:Hampshire,
			  });

			markerHampshire.setMap(mapHampshire);

			var infowindow = new google.maps.InfoWindow({
			  content:"Hampshire"
			  });

			google.maps.event.addListener(markerHampshire, 'click', function() {
			  infowindow.open(mapHampshire,markerHampshire);
			  });
			  
		  
		}

		google.maps.event.addDomListener(window, 'load', initialize);